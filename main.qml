import QtQuick 2.12
import QtQuick.Window 2.12

Item {
    id: main
    height: 480
    width: 640

    RadioChart {
        id: radioChart
        height: parent.height
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }

}


