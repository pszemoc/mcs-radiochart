import QtQuick 2.12
import QtCharts 2.3
import QtQuick.Controls 2.12

Item {
  ChartView {
    id: chartView
    height: parent.height - 40;
    anchors.top: parent.top
    anchors.bottom: radioTimeSpan.top
    anchors.left: parent.left
    anchors.right: parent.right
    antialiasing: true
    property int timeSpan: 60

    ValueAxis {
      id: axisY1
      titleText: "SNR [dB]"
      min: -25
      max: 15
    }

    DateTimeAxis {
      id: axisTime
      format: "HH:mm:ss"
      min: new Date(Date.now())
      max: new Date(Date.now() + 60000)
    }

    LineSeries {
      id: snrSeries
      axisX: axisTime
      axisY: axisY1
      name: "Radio SNR"
      useOpenGL: true
      onPointAdded: {
        if (snrSeries.at(index).x > axisTime.max) {
          axisTime.max = new Date(snrSeries.at(index).x);
          axisTime.min = new Date(snrSeries.at(index).x - timeSpan*1000);
        }
      }
    }

    Connections {
      target: radio_link
      onSnr_notify: {
          radio_link.update_snr(snrSeries);
      }
    }

    function setTimeSpan(value) {
      timeSpan = value;
      axisTime.min = new Date(axisTime.max.getTime() - timeSpan*1000);
    }
  }

  Slider {
    id: radioTimeSpan
    anchors.top: chartView.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom

    from: 60.0
    value: 60.0
    to: 3600.0
    stepSize: 1.0

    onMoved: chartView.setTimeSpan(value)
  }
}

