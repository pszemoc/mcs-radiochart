#include "radio_link_data_provider.h"
#include "telemetry/snr_frame.hpp"
#include <QtCharts/QXYSeries>
#include <QDateTime>
#include <algorithm>

radio_link_data_provider::radio_link_data_provider(QQuickView *appViewer, zmq::context_t& context, QObject *parent)
: QObject(parent)
, appViewer(appViewer)
, context(context)
{
  listener = new socket_listener(context);
  listener->moveToThread(&listener_thread);
  connect(&listener_thread, &QThread::started, listener, &socket_listener::process);
  connect(listener, &socket_listener::snr_received, this, &radio_link_data_provider::push_snr);
  listener_thread.start();
}

void radio_link_data_provider::push_snr(const qint32 &value) {
  snr_vector.push_back(QPointF( static_cast<qreal>(QDateTime::currentMSecsSinceEpoch()), value));
  emit snr_notify();
}

void radio_link_data_provider::update_snr(QAbstractSeries *series) {
    if (series) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        xySeries->append(snr_vector.back());
    }
}

socket_listener::socket_listener(zmq::context_t &context) : context(context) {}

void socket_listener::process() {
  /* MOCK
  qint32 snr = 0;
  while (true) {
    QThread::msleep(1000);
    snr++;
    if (snr > 10) {
        snr = -20;
    }
    emit snr_received(snr);
  }*/

  socket = new zmq::socket_t(context, ZMQ_SUB);
  socket->connect("tcp://localhost:15000");
  socket->setsockopt(ZMQ_SUBSCRIBE, frame_topic::snr.c_str(), frame_topic::snr.size());
  snr_frame_t snr_frame;
  while (true) {
    zmq::message_t topic;
    socket->recv(topic);
    std::string topic_str = std::string(static_cast<char*>(topic.data()), topic.size());
    if (topic_str == frame_topic::snr) {
      socket->recv(zmq::mutable_buffer(&snr_frame, sizeof(snr_frame_t)));
      emit snr_received(snr_frame.snr);
    }
  }
}
