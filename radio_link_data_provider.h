#ifndef RADIO_LINK_DATA_PROVIDER_H
#define RADIO_LINK_DATA_PROVIDER_H

#include <QObject>
#include <QThread>
#include <QVector>
#include <QPointF>
#include <QtCharts/QAbstractSeries>
#include <zmq.hpp>

QT_BEGIN_NAMESPACE
class QQuickView;
QT_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class socket_listener : public QObject {
    Q_OBJECT
private:
    zmq::context_t& context;
    zmq::socket_t* socket;
public:
    socket_listener(zmq::context_t& context);
public slots:
    void process();
signals:
    void snr_received(const qint32&);
};

class radio_link_data_provider : public QObject {
    Q_OBJECT
private:
    QQuickView*       appViewer;
    zmq::context_t&   context;
    socket_listener*  listener;
    QThread           listener_thread;
    QVector<QPointF>  snr_vector;
public:
    explicit radio_link_data_provider(QQuickView* appViewer, zmq::context_t& context, QObject *parent = nullptr);

signals:
    void snr_notify();

public slots:
    void push_snr(const qint32& value);
    void update_snr(QAbstractSeries* series);
};

#endif // RADIO_LINK_DATA_PROVIDER_H
